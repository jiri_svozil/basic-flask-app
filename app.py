from flask import Flask, render_template





flask_app = Flask(__name__)

@flask_app.route('/')
def welcome_page():
    return render_template("hello.html")

@flask_app.route('/about')
def about_page():
    data = {"name": "Hello from Flask"}
    return render_template("about.jinja2", data=data)


